#include <Arduino.h>
#include <GyverWDT.h>
#include <avr/sleep.h>

#define DEBUG // For debugging show the LED status show status of resetting UPS Hat

#define pinExtPower 2 // Pin for check external power on UPS Hat
#define pinRPiIsUP 0  // Pin for chexk is OS on Raspberry Pi is running

#define pinResetUPS 4 // Pin for on/off UPS Hat

#ifdef DEBUG
#define led 1 // Indicator LED to show we're going to reboot the Pi
#define ledON HIGH
#define ledOFF LOW
// States of State Mashine: from one to four
#define stateOne 1
#define stateTwo 2
#define stateThree 3
#define stateFour 4
#define stateUnknown 10
#endif

// Status of OS on RPi
#define OS_IS_UP 1
#define OS_IS_DOWN 0

// Status of external power on UPS Hat
#define EXT_POWER_IS_ON 1
#define EXT_POWER_IS_OFF 0

// Status of resetting UPS Hat
#define UPS_HAT_NORMAL LOW
#define UPS_HAT_RESET HIGH

// For input statuses
#define ON 1
#define OFF 0

#ifdef DEBUG
void ShowState(uint8_t state); // Bink led number state
#endif
void UPSReset(); // Resetting UPS Hat
void isrWDT();
void delayWDT(uint8_t prescaler);


uint8_t vpt;
uint8_t vpp;
uint8_t ost;
uint8_t osp;

void setup()
{
  uint8_t loopdelay = 0;

  set_sleep_mode(SLEEP_MODE_PWR_DOWN);

  pinMode(pinExtPower, INPUT);
  pinMode(pinRPiIsUP, INPUT);

  // Set the 'reset' pin LOW before the mode to avoid glitches
  digitalWrite(pinResetUPS, UPS_HAT_NORMAL);
  pinMode(pinResetUPS, OUTPUT);

#ifdef DEBUG
  pinMode(led, OUTPUT);
  for (int i = 1; i <= 5; i++)
  {
    digitalWrite(led, ledON);
    delayWDT(WDT_PRESCALER_4); // delay ~32ms
    digitalWrite(led, ledOFF);
    delayWDT(WDT_PRESCALER_4); // delay ~32ms
  }
  digitalWrite(led, UPS_HAT_NORMAL);
#endif
  vpt = digitalRead(pinExtPower);
  vpp = vpt;
  ost = digitalRead(pinRPiIsUP);
  osp = ost;
  if ((vpt == ON) & (ost == OFF))
  {
    while (loopdelay < 29)
    {
      loopdelay++;
      delayWDT(WDT_PRESCALER_128); // ~1s
    }
  }
  vpp = vpt;
  osp = ost;
  vpt = digitalRead(pinExtPower);
  ost = digitalRead(pinRPiIsUP);
  if ((vpt == ON) & (ost == OFF))
  {
    UPSReset();
  }
  delayWDT(WDT_PRESCALER_128); // delay ~1s
}

void loop()
{
  vpp = vpt;
  osp = ost;
  vpt = digitalRead(pinExtPower);
  ost = digitalRead(pinRPiIsUP);
  if ((vpt == vpp) & (ost == osp))
  {
    delayWDT(WDT_PRESCALER_128); // delay ~1s
  }
  else
  {
    if (((vpt == ON) & (ost == ON) & (vpp == OFF) & (osp == ON)) |
        ((vpt == ON) & (ost == ON) & (vpp == ON) & (osp == OFF))) // State 1 - NOP
    {
#ifdef DEBUG
      ShowState(stateOne);
#endif
    }
    else if (((vpt == ON) & (ost == OFF) & (vpp == OFF) & (osp == OFF)) |
             ((vpt == ON) & (ost == OFF) & (vpp == ON) & (osp == ON))) // State 2 - Reset
    {
#ifdef DEBUG
      ShowState(stateTwo);
#endif
      UPSReset();
    }
    else if ((vpt == OFF) & (ost == ON) & (vpp == ON) & (osp == ON)) // State 3 - NOP
    {
#ifdef DEBUG
      ShowState(stateThree);
#endif
    }
    else if ((vpt == OFF) & (ost == OFF) & (vpp == OFF) & (osp == ON)) // State 4 Case 1 - Reset
    {
#ifdef DEBUG
      ShowState(stateFour);
#endif
      UPSReset();
    }
    else if ((vpt == OFF) & (ost == OFF) & (vpp == ON) & (osp == OFF)) // State 4 Case 2 - NOP
    {
#ifdef DEBUG
      ShowState(stateFour);
#endif
    }
    else
    {
#ifdef DEBUG
      ShowState(stateUnknown);
#endif
    }
    delayWDT(WDT_PRESCALER_128); // delay ~1s
  }
}

#ifdef DEBUG
void ShowState(uint8_t state)
{
  for (int i = 1; i <= state; i++)
  {
    digitalWrite(led, ledON);
    delayWDT(WDT_PRESCALER_32); // delay ~0.25s
    digitalWrite(led, ledOFF);
    delayWDT(WDT_PRESCALER_32); // delay ~0.25s
  }
  delayWDT(WDT_PRESCALER_128); // delay ~1s
}
#endif

void UPSReset()
{
  delayWDT(WDT_PRESCALER_1024); // delay ~8s
  digitalWrite(pinResetUPS, UPS_HAT_RESET);
#ifdef DEBUG
  digitalWrite(led, UPS_HAT_RESET);
#endif
  delayWDT(WDT_PRESCALER_64); // delay ~0.5s
  digitalWrite(pinResetUPS, UPS_HAT_NORMAL);
#ifdef DEBUG
  digitalWrite(led, UPS_HAT_NORMAL);
#endif
  delayWDT(WDT_PRESCALER_128);  // delay ~1s
  digitalWrite(pinResetUPS, UPS_HAT_RESET);
#ifdef DEBUG
  digitalWrite(led, UPS_HAT_RESET);
#endif
  delayWDT(WDT_PRESCALER_512);  // delay ~4s
  digitalWrite(pinResetUPS, UPS_HAT_NORMAL);
#ifdef DEBUG
  digitalWrite(led, UPS_HAT_NORMAL);
#endif
}

void delayWDT(uint8_t prescaler)
{
  watchdog_enable(INTERRUPT_MODE, prescaler, *isrWDT);
  sleep_enable();
  sleep_cpu();
}

void isrWDT()
{
  sleep_disable();    // Запрещаем сон
  watchdog_disable(); // Выключаем watchdog
}
